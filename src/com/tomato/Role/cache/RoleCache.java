package com.tomato.Role.cache;

import java.util.Collection;

import com.tomato.bean.Role;

public interface RoleCache {
	
	public Collection<Role> getList();

	public Role getById(int id);

	public Role getByName(String name);
}
