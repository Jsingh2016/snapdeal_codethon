package com.tomato.Role.cache.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tomato.Role.cache.RoleCache;
import com.tomato.Role.dao.RoleDao;
import com.tomato.bean.Role;

@Service
public class RoleCacheImpl implements RoleCache {

	@Autowired
	private RoleDao dao;
	
	private Map<Integer,Role> roleMap;
	
	private Map<String,Integer> roleNameMap;
	
	@PostConstruct
	private void init(){
		roleMap = new ConcurrentHashMap<>();
		roleNameMap = new ConcurrentHashMap<>();
		Collection<Role> list = dao.getList();
		for(Role role:list){
			roleMap.put(role.getId(), role);
			roleNameMap.put(role.getName(), role.getId());
		}
	}
	@Override
	public Collection<Role> getList() {
		return Collections.unmodifiableCollection(roleMap.values()) ;
	}

	@Override
	public Role getById(int id) {
		return roleMap.get(id);
	}
	@Override
	public Role getByName(String name) {
		
		int roleId = roleNameMap.get(name);
		return roleMap.get(roleId);
	}

}
