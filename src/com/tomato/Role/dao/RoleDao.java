package com.tomato.Role.dao;

import java.util.Collection;

import com.tomato.bean.Role;

public interface RoleDao {
	
	public Collection<Role> getList();
	
	public Role getById(int id);
}
