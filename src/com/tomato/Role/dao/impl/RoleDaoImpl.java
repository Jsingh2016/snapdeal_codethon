package com.tomato.Role.dao.impl;

import java.util.Collection;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tomato.Role.dao.RoleDao;
import com.tomato.bean.Role;


@Repository
@Transactional
public class RoleDaoImpl implements RoleDao {
	@Autowired
	SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Role> getList() {
		return sessionFactory.openSession().createCriteria(Role.class).list();
	}

	@Override
	public Role getById(int id) {
		return (Role) sessionFactory.openSession().get(Role.class,id);
	}

}
