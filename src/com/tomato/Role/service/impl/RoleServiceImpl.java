package com.tomato.Role.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tomato.Role.cache.RoleCache;
import com.tomato.Role.service.RoleService;
import com.tomato.bean.Role;

@Service
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	private RoleCache cache;
	
	@Override
	public Collection<Role> getList() {
		return cache.getList();
	}

	@Override
	public Role getById(int id) {
		return cache.getById(id);
	}

	@Override
	public Role getByName(String name) {
		return cache.getByName(name);
	}
}
