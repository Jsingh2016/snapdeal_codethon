package com.tomato.Role.service;

import java.util.Collection;

import com.tomato.bean.Role;

public interface RoleService {
	
    public Collection<Role> getList();
	
	public Role getById(int id);
	
	public Role getByName(String name);
	

}
