package com.tomato.auth.bean;

public class LoginStatus {

	private int loginStatus;
	private String statusString;
	private long userId;
	private String userName;
	private int remainingAttempts;
	private int isPasswordReset;

	public static String LOGIN_STATUS_SUCCESS="Success";
	public static String LOGIN_STATUS_FAILURE="Failed";
	public static String LOGIN_STATUS_EXPIRED="Expired";
	public static String LOGIN_STATUS_MAX_NUM_OF_ATTEMPTS="Attempts Exceeded";
	public static String LOGIN_STATUS_CHANGE_PASSWORD="Change Password";
	public static String LOGIN_STATUS_DISABLED="Disable";
	public static String LOGIN_STATUS_INACTIVE="Inactive";
	public static String LOGIN_STATUS_DUPLICATE_EMAIL="Duplicate Email";
	
	public static int LOGIN_STATUS_SUCCESS_CODE=1;
	public static int LOGIN_STATUS_FAILURE_CODE=2;
	public static int LOGIN_STATUS_EXPIRED_CODE=3;
	public static int LOGIN_STATUS_MAX_NUM_OF_ATTEMPTS_CODE=4;
	public static int LOGIN_STATUS_CHANGE_PASSWORD_CODE=5;
	public static int LOGIN_STATUS_DISABLE_CODE=6;
	public static int LOGIN_STATUS_INACTIVE_CODE=7;
	public static int LOGIN_STATUS_DUPLICATE_EMAIL_CODE=8;
	
	public int getIsPasswordReset() {
		return isPasswordReset;
	}
	public void setIsPasswordReset(int isPasswordReset) {
		this.isPasswordReset = isPasswordReset;
	}
	public int getLoginStatus() {
		return loginStatus;
	}
	public void setLoginStatus(int loginStatus) {
		this.loginStatus = loginStatus;
	}
	public String getStatusString() {
		return statusString;
	}
	public void setStatusString(String statusString) {
		this.statusString = statusString;
	}
	
	public long getUserId() {
		return userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getRemainingAttempts() {
		return remainingAttempts;
	}
	public void setRemainingAttempts(int remainingAttempts) {
		this.remainingAttempts = remainingAttempts;
	}
}
