/**
 * 
 */
package com.tomato.auth;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import com.tomato.util.TomatoConstants;



public class TomatoAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
	
	Logger logger = Logger.getLogger(TomatoAuthenticationFailureHandler.class);

	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException, AuthenticationException{
		logger.info("inside toi failure handler");
		if(exception instanceof BadCredentialsException){
			if(StringUtils.isNotEmpty(exception.getMessage()) && (StringUtils.contains(exception.getMessage(), "EXP001")
					|| StringUtils.contains(exception.getMessage(), "EXP002"))) {
				response.sendRedirect(TomatoConstants.INACTIVE_URL);
			} 
		}
	}
	
}