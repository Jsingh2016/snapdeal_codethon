/**
 *
 */
package com.tomato.auth;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

import com.tomato.bean.LoginHistoryBean;
import com.tomato.bean.MenuUrlBean;
import com.tomato.bean.UserBean;
import com.tomato.service.IMasterService;
import com.tomato.service.common.CommonService;
import com.tomato.util.TomatoConstants;

public class TomatoAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

	@Autowired
	private IMasterService masterService;

	@Autowired
	CommonService commonService;
	
	private static final String[] HEADERS_TO_TRY = { 
		    "X-Forwarded-For",
		    "Proxy-Client-IP",
		    "WL-Proxy-Client-IP",
		    "HTTP_X_FORWARDED_FOR",
		    "HTTP_X_FORWARDED",
		    "HTTP_X_CLUSTER_CLIENT_IP",
		    "HTTP_CLIENT_IP",
		    "HTTP_FORWARDED_FOR",
		    "HTTP_FORWARDED",
		    "HTTP_VIA",
		    "REMOTE_ADDR" };
	
    Logger logger = Logger.getLogger(TomatoAuthenticationSuccessHandler.class);

    private RequestCache requestCache = new HttpSessionRequestCache();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
            HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
    	
        SavedRequest savedRequest = requestCache.getRequest(request, response);
        UserBean user = (UserBean) authentication.getPrincipal();
        if (savedRequest != null  && user.getStatus() != TomatoConstants.INACTIVE && 
        		savedRequest.getRedirectUrl().contains(".html") && !StringUtils.isEmpty(savedRequest.getRedirectUrl())) {
        	
        	Map<String, List<MenuUrlBean>> menuMap = masterService.getUserMenuDetails(user.getId());
        	request.getSession().setAttribute("menuDesc", menuMap);
        	/*
        	String deletePriv = masterService.checkDeletePriv(user);
			request.getSession().setAttribute("deletePriv", deletePriv);
			
			String editPriv = masterService.checkEditPriv(user);
			request.getSession().setAttribute("editPriv", editPriv);
        	*/
			response.sendRedirect(savedRequest.getRedirectUrl());
        } else {
                if (user.getStatus() == TomatoConstants.INACTIVE) {
                    response.sendRedirect(TomatoConstants.INACTIVE_URL);
                } else {
                	Map<String, List<MenuUrlBean>> menuMap = masterService.getUserMenuDetails(user.getId());
                	request.getSession().setAttribute("menuDesc", menuMap);
                	/*
                	String deletePriv = masterService.checkDeletePriv(user);
        			request.getSession().setAttribute("deletePriv", deletePriv);
        			
        			String editPriv = masterService.checkEditPriv(user);
        			request.getSession().setAttribute("editPriv", editPriv);
        			
        			*/
        			
        			String ip = getClientIpAddress(request);
        			//GeoLocation loc = GeoLocationUtil.getLocation(ip);
        			
        			LoginHistoryBean history = new LoginHistoryBean();
        			history.setUserId(user.getId());
        			history.setIpAddress(ip);
        			history.setSessionId(request.getSession().getId());
        			/*if(loc != null){
	        			history.setLoggedInCountry(loc.getCountryName());
	        			history.setAddress(loc.toString());
        			}*/
        			commonService.save(history);
        			
                	response.sendRedirect(TomatoConstants.DASHBOARD_URL);
                }
        }
    }

     public void setRequestCache(RequestCache requestCache) {
         this.requestCache = requestCache;
     }

	public static String getClientIpAddress(HttpServletRequest request) {
	    for (String header : HEADERS_TO_TRY) {
	        String ip = request.getHeader(header);
	        if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
	            return ip;
	        }
	    }
	    return request.getRemoteAddr();
	}
     
}