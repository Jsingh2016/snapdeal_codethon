package com.tomato.auth;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.tomato.bean.UserBean;
import com.tomato.service.IMasterService;
import com.tomato.util.TomatoConstants;
public class TomatoInterceptor implements HandlerInterceptor  {
	
	@Autowired
	private IMasterService masterService;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
		throws Exception {
		
		String URL = request.getRequestURI();
		
		if(!URL.contains("login.html") && !URL.contains("index.jsp") && !URL.contains("dashboard.html")&& !URL.contains("logout.html")
				&& !URL.contains("accessDenied.html")){
			
			URL = URL.substring(URL.lastIndexOf("/")+1);
			if(URL.indexOf("?") != -1){
				URL = URL.substring(0,URL.indexOf("?"));
			}
			request.setAttribute("clickedURL", URL);
			/*MenuUrlBean menuUrlBean = masterService.getMenuURLBeanByMenuURL(URL);
			if(menuUrlBean != null){
				request.setAttribute("clickedURLParent", menuUrlBean.getParentInfo());
			}*/
			
			UserBean user = (UserBean) ((Authentication) request.getUserPrincipal()).getPrincipal();
			boolean flag = masterService.checkMenuAccess(URL, user.getId());
			if(!flag){
				
			  boolean ajax = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
		      if (ajax){
		    	  response.sendError(403);
		    	  return false;
		      }
		      else{ 
		    	  response.sendRedirect(TomatoConstants.ACCESS_DENIED_URL);
				  return false;
		      }
				
				
			}
		}
		
		return true;
	}
	@Override
	public void postHandle(	HttpServletRequest request, HttpServletResponse response,
			Object handler, ModelAndView modelAndView) throws Exception {
	}
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
			Object handler, Exception ex) throws Exception {
	}
	
} 