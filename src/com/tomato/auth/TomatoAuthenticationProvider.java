package com.tomato.auth;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;

import com.tomato.auth.bean.LoginStatus;
import com.tomato.bean.UserBean;
import com.tomato.service.IMasterService;


public class TomatoAuthenticationProvider implements AuthenticationProvider {

	Logger LOGGER = Logger.getLogger(TomatoAuthenticationProvider.class);
	public static final String LOG_STRING = "TransportAuthenticationProvider";

	@Autowired
	private IMasterService masterService;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
		System.out.println("in autenticate");
		
		Authentication authenticationToReturn = null;// null would be returned when login was not successful
		Object credentials = authentication.getCredentials();
		Object principals = authentication.getPrincipal();
		String name = authentication.getName();
		
		UserBean user = new UserBean();
		LoginStatus loginStatus = masterService.checkLogin(name, ""+credentials,user);
		LOGGER.debug("login Authentication provider" + loginStatus);
		if(user!=null && user.getId()>0) {
			principals = user;
			
			if (loginStatus!=null && loginStatus.getLoginStatus()==LoginStatus.LOGIN_STATUS_FAILURE_CODE){
				throw new BadCredentialsException("EXP001");//incorrect password
			} else if(loginStatus!=null && loginStatus.getLoginStatus()==LoginStatus.LOGIN_STATUS_DUPLICATE_EMAIL_CODE){
				throw new AuthenticationCredentialsNotFoundException("EXP004");//more than 1 results for the entered email
			} 
			List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
			authenticationToReturn = new UsernamePasswordAuthenticationToken(principals, credentials, grantedAuthorities);
			
		}
		else {
			throw new BadCredentialsException("EXP002");//incorrect username
		}
		return authenticationToReturn;
	}

	@Override
	public boolean supports(Class<? extends Object> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
	
}
