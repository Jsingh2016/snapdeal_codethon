package com.tomato.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.tomato.bean.LoginHistoryBean;
import com.tomato.service.IMasterService;
import com.tomato.service.common.CommonService;

@Controller
public class LoginController {
	

	@Autowired
	IMasterService masterService;
	
	@Autowired
	CommonService commonService;
	
	@RequestMapping(value="login", method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView login(HttpServletRequest request,@RequestParam(value = "error", required = false) boolean errorFailure){
		
		List<String[]> columns = new ArrayList<>();
		String column[] = {"modifiedDate","now()","date"};
		columns.add(column);
		
		commonService.updateColumns(LoginHistoryBean.class, columns, "sessionId='"+request.getSession().getId()+"'");
		
		ModelAndView mav = new ModelAndView("index");
		String referrer = request.getHeader("Referer");
		
		if(referrer!=null){
	        request.getSession().setAttribute("url_prior_login", referrer);
	    }
		
		if (errorFailure == true) { 
			mav.addObject("error", "You have entered an invalid username or password!");
		} else {
			mav.addObject("error", "");
		}
		return mav;
		
	}
	
	@RequestMapping(value="dashboard", method={RequestMethod.POST, RequestMethod.GET})
	public String dashboard(HttpServletRequest request){
		
		return "dashboard";
	}
	
	@RequestMapping(value="accessDenied", method={RequestMethod.POST, RequestMethod.GET})
	public String accessDenied(){
		
		return "accessDenied";
	}
}
