package com.tomato.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tomato.bean.MasterData;
import com.tomato.bean.MenuUrlBean;
import com.tomato.bean.UserBean;
import com.tomato.service.IMasterService;
import com.tomato.service.UserService;
import com.tomato.util.DuplicateDataException;
import com.tomato.util.MasterDataEnum;
import com.tomato.util.MessageConstants;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private IMasterService masterService;

	@Autowired
	private UserService userService;

	@RequestMapping(value = "getUserForm", method = { RequestMethod.GET, RequestMethod.POST })
	public String getUserForm(HttpServletRequest request, Model model) {

		UserBean userBean = new UserBean();
		model.addAttribute("userBean", userBean);

		List<MasterData> statusList = masterService.getMasterData(MasterDataEnum.STATUS);
		model.addAttribute("statusList", statusList);

		List<MenuUrlBean> menuList = masterService.getAllMenuRaw();
		model.addAttribute("menuList", menuList);

		return "addUser";
	}

	@RequestMapping(value = "SaveUser", method = { RequestMethod.GET, RequestMethod.POST })
	public String saveUser(@ModelAttribute("userBean") UserBean userBean, RedirectAttributes redirectAttributes,
			Model model, HttpServletRequest request) {
		UserBean currentUser = (UserBean)((Authentication) request.getUserPrincipal()).getPrincipal();
		try {
			boolean flag = userService.saveUser(userBean, currentUser);
			if (flag) {
				userService.addUserMenuAccess(userBean.getMenuIds(), userBean.getId());
				redirectAttributes.addAttribute("success", MessageConstants.USER_ADD_SUCCESS);
			} else {
				redirectAttributes.addAttribute("error", MessageConstants.ERROR_MSG);
			}
		} catch (DuplicateDataException de) {
			List<MasterData> statusList = masterService.getMasterData(MasterDataEnum.STATUS);
			model.addAttribute("statusList", statusList);

			List<MenuUrlBean> menuList = masterService.getAllMenuRaw();
			model.addAttribute("menuList", menuList);

			model.addAttribute("error", de.getMessage());

			return "addUser";
		}
		if(userBean.getId()==currentUser.getId()){
			return "redirect:"+request.getContextPath()+"/logout.html";
		}
		return "redirect:userList.html";
	}

	@RequestMapping(value = "userList", method = { RequestMethod.GET, RequestMethod.POST })
	public String userList(Model model, HttpServletRequest request) {
		Map<String, String> parameters = new HashMap<>();
		
		List<UserBean> userList = userService.userList(parameters);
		model.addAttribute("userList", userList);

		return "userList";
	}

	@RequestMapping(value = "editUser", method = { RequestMethod.GET, RequestMethod.POST })
	public String editUser(HttpServletRequest request, @RequestParam int id, Model model) {
		
		Map<String, String> parameters = new HashMap<>();
		parameters.put("USER_ID", "" + id);
		
		model.addAttribute("userBean", userService.getUserById(parameters));

		List<MasterData> statusList = masterService.getMasterData(MasterDataEnum.STATUS);
		model.addAttribute("statusList", statusList);

		List<MenuUrlBean> menuList = masterService.getAllMenuRaw();
		model.addAttribute("menuList", menuList);

		return "addUser";
	}

	@RequestMapping(value = "deleteUser", method = { RequestMethod.GET, RequestMethod.POST })
	public String deleteUser(HttpServletRequest request, @RequestParam int id, Model model) {
		
		Map<String, String> parameters = new HashMap<>();
		parameters.put("USER_ID", "" + id);
		
		userService.deleteUser(parameters);

		return "redirect:userList.html";
	}

	@RequestMapping(value = "viewUser", method = { RequestMethod.GET, RequestMethod.POST })
	public String viewUser(HttpServletRequest request, @RequestParam int id, Model model) {
		
		Map<String, String> parameters = new HashMap<>();
		parameters.put("USER_ID", "" + id);
		
		model.addAttribute("userBean", userService.getUserById(parameters));

		return "viewUser";
	}
	
}
