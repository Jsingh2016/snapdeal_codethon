package com.tomato.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tomato.service.common.CommonService;

@Controller
@RequestMapping("/master")
public class MasterController {

	private static final Logger LOGGER = Logger.getLogger(MasterController.class);
	
	@Autowired
	CommonService commonService;

	@RequestMapping(value="user", method={RequestMethod.GET, RequestMethod.POST})
	public String user(){
		
		return "userList";
	}	
}

