package com.tomato.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tomato.bean.MasterData;
import com.tomato.bean.ModuleDetail;
import com.tomato.bean.Role;
import com.tomato.bean.StatusBean;
import com.tomato.dao.IMasterDao;
import com.tomato.util.MasterDataEnum;

@Repository
@Transactional
public class MasterDaoImpl implements IMasterDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<MasterData> getMasterData(MasterDataEnum typeEnum) {
		String type = typeEnum.name();
		
		@SuppressWarnings("unchecked")
		List<MasterData> list = sessionFactory.getCurrentSession().createCriteria(MasterData.class).add(Restrictions.eq("type", type)).list();
		
		return list;
	}
	
	@Override
	public List<MasterData> getAllMasterData() {
		@SuppressWarnings("unchecked")
		List<MasterData> masterDataList = sessionFactory.getCurrentSession().createCriteria(MasterData.class).list();
		
		return masterDataList;
	}
	
	@Override
	public List<ModuleDetail> getModuleDetail(){
		
		@SuppressWarnings("unchecked")
		List<ModuleDetail> moduleList = sessionFactory.getCurrentSession().createCriteria(ModuleDetail.class).list();
		
		return moduleList;
		
	}
	
	@Override
	public List<StatusBean> getStatus(){
		
		@SuppressWarnings("unchecked")
		List<StatusBean> statusList = sessionFactory.getCurrentSession().createCriteria(StatusBean.class).list();
		
		return statusList;
		
	}

	@Override
	public Role getRoleById(int roleId){
		
		return (Role)sessionFactory.getCurrentSession().get(Role.class, roleId);
		
	}
	
	@Override
	public Collection<Role> getUserRole(int userId) {
		
		Collection<Role> col = new ArrayList<>();
		
		Query qry = sessionFactory.getCurrentSession().createQuery("select roleId from UserRole where userId = :userId");
		qry.setParameter("userId", userId);
		@SuppressWarnings("unchecked")
		List<Integer> list = qry.list();
		Iterator<Integer> itr = list.iterator();
		while(itr.hasNext()){
			int id = itr.next();
			Role role = getRoleById(id);
			
			if(role != null){
				col.add(role);
			}
		}
		
		return col;
	}
	
}
