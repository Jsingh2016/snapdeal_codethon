package com.tomato.dao;

import java.util.Collection;
import java.util.List;

import com.tomato.bean.MasterData;
import com.tomato.bean.ModuleDetail;
import com.tomato.bean.Role;
import com.tomato.bean.StatusBean;
import com.tomato.util.MasterDataEnum;

public interface IMasterDao {
	
	public List<MasterData> getMasterData(MasterDataEnum type);
	
	public List<MasterData> getAllMasterData();
	
	public List<ModuleDetail> getModuleDetail();
	
	public List<StatusBean> getStatus();
	
	public Collection<Role> getUserRole(int userId);
	
	public Role getRoleById(int roleId);
}
