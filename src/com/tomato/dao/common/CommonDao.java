package com.tomato.dao.common;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public interface CommonDao {

	public boolean save(Object obj);
	
	public boolean isExists(final String beanName,final String whereCondition);
	
	public <T> T get(Class<T> t, Serializable key);
	
	public List<HashMap<String, String>> getSpecificColumn(String beanName, List<String> columns, String whereCondition);
	
	public <T> List<T> getAllData(Class<T> T);
	
	public <T> List<T> getDataByWhereCondition(Class<T> T, String whereCond);
	
	public <T> List<T> getDataPagination(Class<T> T, int startIndex, int maxResult);
	
	public <T> void deleteData(Class<T> T, String whereCondition);
	
	public <T> List<Object> getSingleColumnList(Class<T> T,String columnName, String whereCondition);
	
	public <T> void updateColumns(Class<T> T, List<String[]> columns, String whereCondition);
	
	public void runHQLDMLQuery(String query);
}
