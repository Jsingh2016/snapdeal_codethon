package com.tomato.util;

public interface TomatoConstants {

	/***********************Document Status********************************/
	
	int ACTIVE = 5;
	int INACTIVE = 6;
	int ALL = -1;
	int DELETED = 5;
	
	
	/***********************Module's Names********************************/
	
	
	/***********************INVENTORY TYPE********************************/
	
	
	/***********************Dates Format********************************/
	
	String DISPLAY_DATE_FORMAT = "dd/MM/yyyy";
	String DISPLAY_DATE_FORMAT_JS = "dd/mm/yy";

	
	/***********************User Roles ********************************/
	
	static final String ROLE_ADMIN									="ADMIN";
	static final String ROLE_TOP_MANAGEMENT							="TOP_MANAGEMENT";
	static final String ROLE_MIDDLE_MANAGEMENT						="MIDDLE_MANAGEMENT";
	static final String ROLE_PRIVILEGED_MIDDLE_MANAGEMENT			="PRIVILEGED_MIDDLE_MANAGEMENT";
	static final String ROLE_BOTTOM_MANAGEMENT						="BOTTOM_MANAGEMENT";
	static final String ROLE_FINANCE_MANAGEMENT						="FINANCE_MANAGEMENT";
	
	static final int DISPLAY_AS_MENU_Y = 1;
	static final int DISPLAY_AS_MENU_N = 0;
	static final int DISPLAY_AS_MENU_SKIP = 2;
	
	/***********************User Roles Id********************************/
	
	static final int ADMIN_ID										=4;
	static final int TOP_MANAGEMENT									=1;
	static final int MIDDLE_MANAGEMENT								=2;
	static final int PRIVILEGED_MIDDLE_MANAGEMENT					=5;
	static final int BOTTOM_MANAGEMENT								=3;
	
	static final String INACTIVE_URL = "/Tomato/login.html?errorInactive="+true;
	static final String DASHBOARD_URL = "/Tomato/dashboard.html";
	static final String ACCESS_DENIED_URL = "/Tomato/accessDenied.html";
	
	static final int APPLY_REBATE_YES = 1;
	static final int APPLY_REBATE_NO = 0;
	
	static String ATTACHMENT_TYPE_DOC = "DOC";
	static String ATTACHMENT_TYPE_PIC = "PIC";
	static String ITEM_DOCUMENT = "item";
	
	
}
