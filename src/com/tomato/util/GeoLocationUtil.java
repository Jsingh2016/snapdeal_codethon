package com.tomato.util;

import java.io.IOException;
import java.net.InetAddress;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.maxmind.geoip.LookupService;

public class GeoLocationUtil {

	private static LookupService lookUp;

    static {
        try {
        	Resource resource = new ClassPathResource("com/transport/util/GeoLiteCity.dat");
        	lookUp = new LookupService(resource.getFile(),LookupService.GEOIP_MEMORY_CACHE);
            System.out.println("GeoIP Database loaded: " + lookUp.getDatabaseInfo());
        } catch (IOException e) {
            System.out.println("Could not load geo ip database: " + e.getMessage());
        }
    }

    public static GeoLocation getLocation(String ipAddress) {
    	System.out.println("ipAddress : " + ipAddress);
        return GeoLocation.map(lookUp.getLocation(ipAddress));
    }

    public static GeoLocation getLocation(long ipAddress){
        return GeoLocation.map(lookUp.getLocation(ipAddress));
    }

    public static GeoLocation getLocation(InetAddress ipAddress){
        return GeoLocation.map(lookUp.getLocation(ipAddress));
    }
    
}
