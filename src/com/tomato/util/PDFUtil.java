package com.tomato.util;

import java.sql.Connection;
import java.util.Map;

import org.apache.log4j.Logger;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;


public class PDFUtil {
	
	private static final Logger logger = Logger.getLogger(PDFUtil.class.getName());
    
   /* public static void generateReport(List<Map<String, String>> data){
    	try{
	    	JasperDesign design = getJasperDesign(data);
	        JasperReport jasperReport = JasperCompileManager.compileReport(design);
	        
	        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,null, new JREmptyDataSource() );
	        
	        JasperViewer.viewReport(jasperPrint);
	        String path = "C:/Users/Vivek1/Desktop/Jasper Test/GridReportSample.pdf";
	        FileOutputStream fos = new FileOutputStream(path);
	        JasperExportManager.exportReportToPdfStream(jasperPrint, fos);
	        
    	}
    	catch (Exception e) {
			System.out.println(e);
		}
    }*/
    
    public static byte[] generateReportFromJRXML(String filePath, Map<String, Object> parameters, Connection conn, String fileName){
    	byte reportBytes[] = null;
        try {
			JasperPrint jasperPrint = JasperFillManager.fillReport(filePath, parameters, conn);
	        jasperPrint.setName(fileName+".pdf");
	        reportBytes = JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (JRException e) {
			e.printStackTrace();
			logger.error(e);
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}
       
		return reportBytes;
    }
    /*
    private static JasperDesign getJasperDesign(List<Map<String, String>> data)
    { 
         //JasperDesign 
        JasperDesign jasperDesign = null;
        try{
            int reportWidth = 1000;
            
            jasperDesign = new JasperDesign(); 
            jasperDesign.setName("Report"); 
            jasperDesign.setPageWidth(reportWidth+100); 
            jasperDesign.setPageHeight(842); 
            jasperDesign.setColumnWidth(reportWidth); 
            jasperDesign.setColumnSpacing(0); 
            jasperDesign.setLeftMargin(10); 
            jasperDesign.setRightMargin(10); 
            jasperDesign.setTopMargin(20); 
            jasperDesign.setBottomMargin(20); 

            //Fonts 
            JRDesignReportFont normalFont = new JRDesignReportFont(); 
            normalFont.setName("Arial_Normal"); 
            normalFont.setDefault(true); 
            normalFont.setFontName("Arial"); 
            normalFont.setSize(12); 
            normalFont.setPdfFontName("Helvetica"); 
            normalFont.setPdfEncoding("Cp1252"); 
            normalFont.setPdfEmbedded(false); 
            jasperDesign.addFont(normalFont); 

            JRDesignReportFont headerFont = new JRDesignReportFont(); 
            headerFont.setName("Arial_1Normal"); 
            headerFont.setDefault(false); 
            headerFont.setFontName("Arial"); 
            headerFont.setSize(15); 
            headerFont.setBold(true); 
            headerFont.setPdfFontName("Helvetica-Bold"); 
            headerFont.setPdfEncoding("Cp1252"); 
            headerFont.setPdfEmbedded(false); 
            jasperDesign.addFont(headerFont); 
                    
            JRDesignReportFont bigFont = new JRDesignReportFont(); 
            bigFont.setName("Big_font"); 
            bigFont.setDefault(true); 
            bigFont.setFontName("Arial"); 
            bigFont.setSize(22); 
            bigFont.setPdfFontName("Helvetica"); 
            bigFont.setPdfEncoding("Cp1252"); 
            bigFont.setPdfEmbedded(false); 
            bigFont.setBold(true);
            jasperDesign.addFont(bigFont); 
            

            //Query 
            JRDesignQuery query = new JRDesignQuery(); 
            
             *//******************  Dynamic query generation ends here   ************************//*
            
            query.setText(""); 
            jasperDesign.setQuery(query); 
                        
            *//*********** Dynamic field generation ends here ******************//*
            
           //Title 
            JRDesignBand band = new JRDesignBand(); 
             
            JRDesignStaticText staticText = new JRDesignStaticText();
            staticText.setX(reportWidth/2 - 100); 
            staticText.setY(0); 
            staticText.setWidth(200); 
            staticText.setHeight(30); 
            staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_CENTER); 
            
            staticText.setFont(bigFont); 
            staticText.setText("Declaration"); 
            band.addElement(staticText); 
            
            JRDesignLine line = new JRDesignLine(); 
            line.setX(0); 
            line.setY(32); 
            line.setWidth(reportWidth); 
            line.setHeight(0); 
            band.addElement(line);
            
            
            *//******************** Dynamic selection generation   ***************************//*
            
            int x = 0;
            int y = 30;
            
            band.setHeight(y+10); 
            jasperDesign.setTitle(band); 
            
            //y = y+ 10;
            
            //Page header 
            band = new JRDesignBand(); 
            band.setHeight(40);
            *//******************** Dynamic header label generation   ***************************//*
            
            
            *//******************** Dynamic header label generation ends here   ***************************//*
            
            int width=100;
            int height = 30;
            
            //Column header 
            band = new JRDesignBand(); 
            jasperDesign.setColumnHeader(band); 

            //Detail 
            band = new JRDesignBand(); 
            band.setHeight(30); 
            
            x = 0;
            y = 0;
            
            *//***************  Dynamic detail generation  ***********************//*
            
            Iterator<Map.Entry<String, String>> itr = data.get(0).entrySet().iterator();
            while(itr.hasNext()){
            	
            	Map.Entry<String, String> me = itr.next();
            	
            	staticText = new JRDesignStaticText(); 
                staticText.setX(x); 
                staticText.setY(y); 
                staticText.setWidth(width); 
                staticText.setHeight(height); 
                staticText.setForecolor(Color.black);  
                staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT); 
                staticText.setVerticalAlignment(JRTextElement.VERTICAL_ALIGN_MIDDLE);
                staticText.setFont(headerFont); 
                staticText.setText(me.getKey()); 
                band.addElement(staticText); 
                
                x = x + width+2;
            }
            
            x = 0;
            y = y+height;
            
            Iterator<Map<String, String>> itrDetail = data.iterator();
            while(itrDetail.hasNext()){
            	Map<String, String> map = itrDetail.next();
            	Iterator<Map.Entry<String, String>> itrMap = map.entrySet().iterator();
            	while(itrMap.hasNext()){
            		Map.Entry<String, String> me = itrMap.next();
            		
            		staticText = new JRDesignStaticText(); 
                    staticText.setX(x); 
                    staticText.setY(y); 
                    staticText.setWidth(width); 
                    staticText.setHeight(height); 
                    staticText.setForecolor(Color.black);  
                    staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT); 
                    staticText.setVerticalAlignment(JRTextElement.VERTICAL_ALIGN_MIDDLE);
                    staticText.setFont(headerFont); 
                    staticText.setText(me.getValue()); 
                    band.addElement(staticText); 
            		
                    x = x + width;
            	}
            	y = y + height;
            	
            }
            
            *//***************  Dynamic detail generation  ***********************//*
            jasperDesign.setDetail(band); 

            //Column footer 
            band = new JRDesignBand(); 
            jasperDesign.setColumnFooter(band); 

            //Page footer 
            band = new JRDesignBand(); 
            jasperDesign.setPageFooter(band); 

            //Summary 
            band = new JRDesignBand(); 
            jasperDesign.setSummary(band); 
             
        }
         catch(JRException jre){
            System.out.println("jre exception"+jre);
        }
        return jasperDesign;
    }*/
	
}
