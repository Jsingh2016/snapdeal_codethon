package com.tomato.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/*@ControllerAdvice*/
public class GenericExceptionHandler {

	public GenericExceptionHandler(){
		
	}
	/*
	@ExceptionHandler(value = Exception.class)
	public ModelAndView handleCustomException(DuplicateDataException ex) {

		ModelAndView model = new ModelAndView("error/generic_error");
		model.addObject("errMsg", ex.getMessage());

		return model;

	}*/
	
	@ExceptionHandler(value = Exception.class)
	public ModelAndView handleCustomException(Exception ex,HttpServletRequest req) {
		
		ModelAndView model = new ModelAndView("error/generic_error");
		//model.addObject("errCode", ex.getErrCode());
		model.addObject("errMsg", ex.getMessage());

		return model;

	}

	
}
