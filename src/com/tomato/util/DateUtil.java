package com.tomato.util;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateFormatUtils;

import com.ibm.icu.text.SimpleDateFormat;

public class DateUtil {

	public static String displayDateinDDMMYYYY(Date date){
		
		String dateStr = "";
		if(date == null){
			return dateStr;
		}
		try{
			dateStr = DateFormatUtils.format(date, TomatoConstants.DISPLAY_DATE_FORMAT); 
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
		return dateStr;
	}
	
	public static Date convertDate(String date){
		Date dateStr = null;
		if(date == null){
			return dateStr;
		}
		try{
			SimpleDateFormat sdf = new SimpleDateFormat(TomatoConstants.DISPLAY_DATE_FORMAT);
			dateStr = sdf.parse(date);
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
		
		return dateStr;
	}
	
	
	public static Date getCurrentDate() {
		Calendar today = Calendar.getInstance();
		return today.getTime();
	}
	
	public static String getCurrentDateTimeForAttachment(){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date now = new Date();
		String current = sdf.format(now);
		current = current.replaceAll("\\ ", "_");
		current = current.replaceAll("\\:", "_");
		current = current.replaceAll("\\.", "_");
		current = current.replaceAll("\\-", "_");
		
		return "_"+current;
	}
	
}
