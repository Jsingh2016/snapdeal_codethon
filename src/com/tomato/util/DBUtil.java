package com.tomato.util;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DBUtil {

	public DBUtil(){
		
	}
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean isExists(String tableName, String whereCondition){
	
		@SuppressWarnings("rawtypes")
		List l = sessionFactory.openSession().createQuery("from " + tableName + " where " + whereCondition).list();
		if(l != null && l.size() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public String getSingleColumn(String columnName, String tableName, String whereCondition){
		
		String query = "select "+columnName+" from " + tableName;
		if(whereCondition != null && !whereCondition.equals("")){
			query = query +  " where " + whereCondition;
		}
		String value = "";
		try{
			//value = jdbcTemplate.queryForObject(query, String.class);
		}
		catch (Exception e) {
			value = "";
		}
		return value;
	}
	
	public boolean insertRecord(String query, Object args[]){
		
		boolean flag = false;
		/*int i = jdbcTemplate.update(query, args);
		if(i > 0){
			flag = true;
		}*/
		
		return flag;
	}
	
	public boolean updateRecord(String query, Object args[]){
		
		boolean flag = false;
		/*int i = jdbcTemplate.update(query, args);
		if(i > 0){
			flag = true;
		}*/
		
		return flag;
	}
	
	/*public static void closePreparedStatement(PreparedStatement pstmt){
		if(pstmt != null){
			try {
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void closeConnection(Connection con){
		if(con != null){
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	*/
}
