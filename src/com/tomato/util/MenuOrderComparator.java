package com.tomato.util;

import java.util.Comparator;
import java.util.Map;

@SuppressWarnings("rawtypes")
public class MenuOrderComparator implements Comparator{

	Map<String, Integer> menuOrderMap;
	public MenuOrderComparator(Map<String, Integer> menuOrderMap){
		this.menuOrderMap = menuOrderMap;
	}
	@Override
	public int compare(Object o1, Object o2) {
		if(o1 instanceof String && o2 instanceof String){
			String p1 = (String)o1;
			String p2 = (String)o2;
			if(menuOrderMap.get(p1) > menuOrderMap.get(p2)){
				return 1;
			}
			else if(menuOrderMap.get(p1) < menuOrderMap.get(p2)){
				return -1;
			}
			else{
				return 0;
			}
		}
		else{
			return 1;
		}
	}

}
