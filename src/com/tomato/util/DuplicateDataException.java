package com.tomato.util;

public class DuplicateDataException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String msg = "";
	
	public DuplicateDataException(){
		this.msg = "Duplicate data entered";
	}

	public DuplicateDataException(String msg){
		this.msg = msg;
	}
	
	@Override
	public String getMessage(){
		
		return msg;
	}
	
	@Override
	public String toString(){
		
		return msg;
	}
}
