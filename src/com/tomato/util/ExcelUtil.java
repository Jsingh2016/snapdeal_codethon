package com.tomato.util;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {

	public static XSSFWorkbook exportExcel(List<Map<String, String>> data){
		
		XSSFWorkbook workbookCreate = new XSSFWorkbook(); 
        XSSFSheet sheetCreate = workbookCreate.createSheet("User Data");
        
        int rowNo = 0;
        Row rowHeader = sheetCreate.createRow(rowNo);
		
        // header creation
        Map<String, String> headerMap = data.get(0);
        Iterator<Map.Entry<String, String>> itrHeader = headerMap.entrySet().iterator();
        int i = 0;
        while(itrHeader.hasNext()){
        	 Map.Entry<String, String> entry = itrHeader.next();
	    	 rowHeader.createCell(i).setCellValue(entry.getKey());
	         rowHeader.getCell(i).setCellStyle(getHeaderCellStyle(workbookCreate));
	         i++;
        }
        // header creation ends
        
        rowNo++;
        
        Iterator<Map<String, String>> itrBody = data.iterator();
        while(itrBody.hasNext()){
        	
        	Row row = sheetCreate.createRow(rowNo);
        	i = 0;
        	Map<String, String> map = itrBody.next();
        	Iterator<Map.Entry<String, String>> itr = map.entrySet().iterator();
        	while(itr.hasNext()){
        		Map.Entry<String, String> me = itr.next();
        		row.createCell(i).setCellValue(me.getValue());
        		i++;
        	}
        	rowNo++;
        }
        
        headerMap = data.get(0);
        itrHeader = headerMap.entrySet().iterator();
        i = 0;
        while(itrHeader.hasNext()){
        	itrHeader.next();
        	sheetCreate.autoSizeColumn(i);
        	i++;
        }
        
        sheetCreate.createFreezePane(0, 0, 0, 0);
        
        return workbookCreate;
	}
	
	public static CellStyle getHeaderCellStyle(XSSFWorkbook workbook){
		
		XSSFFont font = workbook.createFont();
		font.setColor(IndexedColors.WHITE.getIndex());
	    font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	    
        XSSFCellStyle cellStyle= workbook.createCellStyle();
        cellStyle.setFont(font);
       
        cellStyle.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
         
        cellStyle.setWrapText(true);
        
        return cellStyle;
	}
}
