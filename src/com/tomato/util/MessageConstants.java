package com.tomato.util;

public interface MessageConstants {

	String USER_ADD_SUCCESS="User added successfully.";
	String ERROR_MSG = "There is some error. Please try again later.";
	
}
