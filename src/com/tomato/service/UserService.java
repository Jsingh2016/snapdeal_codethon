package com.tomato.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.tomato.bean.UserBean;

public interface UserService {

	public UserBean getUserByIdAndPassword(String userId, String password);
	
	public List<UserBean> getActiveUser(Map<String, String> parameters);
	
	public boolean saveUser(UserBean userBean, UserBean currentUser);
	
	public List<UserBean> userList(Map<String, String> parameters);
	
	public UserBean getUserById(Map<String, String> parameters);
	
	public void deleteUser(Map<String, String> parameters);
	
	public List<Map<String, String>> getUserForExport(Map<String, String> parameters);
	
	public byte[] getUserDataForPDF(String filePath, Map<String, Object> parameters);
	
	public void addUserMenuAccess(Collection<Integer> menuIds, int userId);
}
