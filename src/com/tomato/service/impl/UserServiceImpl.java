package com.tomato.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tomato.bean.MenuUserMappingBean;
import com.tomato.bean.UserBean;
import com.tomato.service.UserService;
import com.tomato.service.common.CommonService;
import com.tomato.util.DateUtil;
import com.tomato.util.PDFUtil;
import com.tomato.util.TomatoConstants;

@Component
public class UserServiceImpl implements UserService {
	
	private static final Logger logger = Logger.getLogger(UserServiceImpl.class.getName());
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private DataSource dataSource;
	
	@Override
	public UserBean getUserByIdAndPassword(String userId, String password) {
		
		UserBean userBean = null;
		List<UserBean> list = commonService.getDataByWhereCondition(UserBean.class, "id="+Integer.parseInt(userId)+""
				+ " and password='"+password+"'");
		if(list != null && list.size() > 0){
			userBean = list.get(0);
		}
		
		return userBean;
	}

	@Override
	public List<UserBean> getActiveUser(Map<String, String> parameters) {
		List<UserBean> userList = commonService.getDataByWhereCondition(UserBean.class, " status="+TomatoConstants.ACTIVE);
		return userList;
	}

	@Override
	public boolean saveUser(UserBean userBean, UserBean currentUser) {
		userBean.setCreatedBy(currentUser.getId());
		userBean.setModifiedBy(currentUser.getId());
		userBean.setModifiedDate(DateUtil.getCurrentDate());
		userBean.setCreatedDate(DateUtil.getCurrentDate());
		boolean flag = commonService.save(userBean);
		return flag;
	}

	@Override
	public List<UserBean> userList(Map<String, String> parameters) {
		List<UserBean> userList = commonService.getAllData(UserBean.class);
		return userList;
	}

	@Override
	public UserBean getUserById(Map<String, String> parameters) {
		List<UserBean> list = commonService.getDataByWhereCondition(UserBean.class, "id="+Integer.parseInt(parameters.get("USER_ID")));
		if(list != null && list.size() > 0){
			UserBean userBean = list.get(0);
			
			List<Integer> menuIds = new ArrayList<>();
			List<MenuUserMappingBean> listMenuMapping = commonService.getDataByWhereCondition(MenuUserMappingBean.class, "userId="+userBean.getId());
			if(listMenuMapping != null && listMenuMapping.size() > 0){
				java.util.Iterator<MenuUserMappingBean> itr = listMenuMapping.iterator();
				while(itr.hasNext()){
					menuIds.add(itr.next().getMenuId());
				}
			}
			userBean.setMenuIds(menuIds);
			
			return userBean;
		}
		return null;
	}

	@Override
	public void deleteUser(Map<String, String> parameters) {
		
		commonService.runHQLDMLQuery("update UserBean set status="+TomatoConstants.INACTIVE + " where id="+Integer.parseInt(parameters.get("USER_ID")));
		
	}

	@Override
	public List<Map<String, String>> getUserForExport(Map<String, String> parameters) {
		
		List<Map<String, String>> userList = new ArrayList<>();
		
		List<UserBean> list = commonService.getDataByWhereCondition(UserBean.class, "status != "+TomatoConstants.INACTIVE);
		if(list != null && list.size() > 0){
			Iterator<UserBean> itr = list.iterator();
			while(itr.hasNext()){
				UserBean userBean = itr.next();
				Map<String, String> map = new LinkedHashMap<>();
				map.put("User Id", ""+userBean.getId());
				map.put("User Name", ""+userBean.getUserName());
				map.put("Nick Name", ""+userBean.getNickName());
				map.put("Mobile", ""+userBean.getMobile());
				map.put("Recovery Email", ""+userBean.getRecoveryEmail());
			
				userList.add(map);
			}
		}
		
		return userList;
	}

	@Override
	public byte[] getUserDataForPDF(String filePath, Map<String, Object> parameters) {
		byte data[] = null;
		try {
			data = PDFUtil.generateReportFromJRXML(filePath, parameters, dataSource.getConnection(),"user");
		} catch (SQLException e) {
			logger.error(e);
		}
		return data;
	}

	@Override
	public void addUserMenuAccess(Collection<Integer> menuIds, int userId) {
		commonService.deleteData(MenuUserMappingBean.class, " userId = "+userId);
		Iterator<Integer> itr = menuIds.iterator();
		while(itr.hasNext()){
			int menuId = itr.next();
			MenuUserMappingBean m = new MenuUserMappingBean();
			m.setMenuId(menuId);
			m.setUserId(userId);
			commonService.save(m);
		}
	}
}
