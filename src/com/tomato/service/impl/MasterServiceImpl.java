package com.tomato.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tomato.auth.bean.LoginStatus;
import com.tomato.bean.MasterData;
import com.tomato.bean.MenuOrder;
import com.tomato.bean.MenuUrlBean;
import com.tomato.bean.MenuUserMappingBean;
import com.tomato.bean.UserBean;
import com.tomato.cache.MasterCache;
import com.tomato.service.IMasterService;
import com.tomato.service.UserService;
import com.tomato.service.common.CommonService;
import com.tomato.util.MasterDataEnum;
import com.tomato.util.MenuOrderComparator;
import com.tomato.util.TomatoConstants;

@Service
public class MasterServiceImpl implements IMasterService {

	private static final Logger logger = Logger.getLogger(MasterServiceImpl.class);
	/*
	 * @Autowired private IMasterDao masterDao;
	 */
	@Autowired
	private MasterCache masterCache;

	@Autowired
	private UserService userService;

	@Autowired
	private CommonService commonService;

	@Override
	public List<MasterData> getMasterData(MasterDataEnum type) {
		return masterCache.getMasterData(type);
	}

	@Override
	public Map<String, List<MenuUrlBean>> getUserMenuDetails(int userId) {

		Map<String, Integer> menuOrderMap = getMenuOrder();
		logger.error("menuOrderMap : " + menuOrderMap);

		@SuppressWarnings("unchecked")
		Map<String, List<MenuUrlBean>> map = new TreeMap<>(new MenuOrderComparator(menuOrderMap));

		//List<MenuUserMappingBean> listUserList = commonService.getDataByWhereCondition(MenuUserMappingBean.class,"userId = " + userId);
		List<MenuUserMappingBean> listUserList = commonService.getDataByWhereCondition(MenuUserMappingBean.class,"");
		
		Iterator<MenuUserMappingBean> itr = listUserList.iterator();
		while (itr.hasNext()) {
			MenuUserMappingBean menuUserMapping = itr.next();
			List<MenuUrlBean> menuUrlBeanList = commonService.getDataByWhereCondition(MenuUrlBean.class,
					"menuId=" + menuUserMapping.getMenuId());
			Iterator<MenuUrlBean> itr1 = menuUrlBeanList.iterator();
			while (itr1.hasNext()) {
				MenuUrlBean menuUrlBean = itr1.next();
				if (menuUrlBean.getDisplayAsMenu() == TomatoConstants.DISPLAY_AS_MENU_Y) {
					if (map.containsKey(menuUrlBean.getParentInfo())) {
						map.get(menuUrlBean.getParentInfo()).add(menuUrlBean);
					} else {
						ArrayList<MenuUrlBean> al = new ArrayList<>();
						al.add(menuUrlBean);

						map.put(menuUrlBean.getParentInfo(), al);
					}
				}
			}

		}

		return map;
	}

	@Override
	public boolean checkMenuAccess(String URL, int userId) {

		List<MenuUrlBean> menuUrlBeanList = commonService.getDataByWhereCondition(MenuUrlBean.class,
				"upper(menuUrl) ='" + URL.toUpperCase() + "'");
		if (menuUrlBeanList != null && menuUrlBeanList.size() > 0) {
			MenuUrlBean menuUrlBean = menuUrlBeanList.get(0);
			if (menuUrlBean.getDisplayAsMenu() == TomatoConstants.DISPLAY_AS_MENU_SKIP) {
				return true;
			}
			else{
				List<MenuUserMappingBean> menuUserMappingList = commonService.getDataByWhereCondition(
						MenuUserMappingBean.class, "userId=" + userId + " and menuId=" + menuUrlBean.getMenuId());
				if (menuUserMappingList != null && menuUserMappingList.size() > 0) {
					return true;
				}
				else{
					return false;
				}
			}
		}
		else{
			return true;
		}
	}

	@Override
	public MenuUrlBean getMenuURLBeanByMenuURL(String URL) {

		List<MenuUrlBean> menuUrlBeanList = commonService.getDataByWhereCondition(MenuUrlBean.class,
				"upper(menuUrl) ='" + URL.toUpperCase() + "'");
		if (menuUrlBeanList != null && menuUrlBeanList.size() > 0) {
			return menuUrlBeanList.get(0);
		} else {
			return null;
		}

	}

	@Override
	public Map<String, List<MenuUrlBean>> getAllMenuDetails() {

		Map<String, List<MenuUrlBean>> map = new HashMap<>();
		List<MenuUserMappingBean> listUserList = commonService.getAllData(MenuUserMappingBean.class);
		Iterator<MenuUserMappingBean> itr = listUserList.iterator();
		while (itr.hasNext()) {
			MenuUserMappingBean menuUserMapping = itr.next();
			List<MenuUrlBean> menuUrlBeanList = commonService.getDataByWhereCondition(MenuUrlBean.class,
					"menuId=" + menuUserMapping.getMenuId());
			Iterator<MenuUrlBean> itr1 = menuUrlBeanList.iterator();
			while (itr1.hasNext()) {
				MenuUrlBean menuUrlBean = itr1.next();
				if (menuUrlBean.getDisplayAsMenu() == TomatoConstants.DISPLAY_AS_MENU_Y) {
					if (map.containsKey(menuUrlBean.getParentInfo())) {
						map.get(menuUrlBean.getParentInfo()).add(menuUrlBean);
					} else {
						ArrayList<MenuUrlBean> al = new ArrayList<>();
						al.add(menuUrlBean);

						map.put(menuUrlBean.getParentInfo(), al);
					}
				}
			}

		}

		return map;
	}

	@Override
	public List<MenuUrlBean> getAllMenuRaw() {

		List<MenuUrlBean> menuList = commonService.getDataByWhereCondition(MenuUrlBean.class,
				" displayAsMenu!=" + TomatoConstants.DISPLAY_AS_MENU_SKIP + " order by parentInfo");

		return menuList;
	}

	private Map<String, Integer> getMenuOrder() {

		Map<String, Integer> menuOrderMap = new HashMap<>();

		List<MenuOrder> list = commonService.getAllData(MenuOrder.class);
		Iterator<MenuOrder> itr = list.iterator();
		while (itr.hasNext()) {
			MenuOrder objMenuOrder = itr.next();
			menuOrderMap.put(objMenuOrder.getParentMenu(), objMenuOrder.getOrder());
		}

		return menuOrderMap;
	}

	@Override
	public LoginStatus checkLogin(String userId, String password, UserBean userBean) {
		LoginStatus loginStatus = new LoginStatus();

		UserBean uBean = userService.getUserByIdAndPassword(userId, password);
		if (uBean != null) {
			try {
				BeanUtils.copyProperties(userBean, uBean);
			} catch (IllegalAccessException | InvocationTargetException e) {
				logger.error(e);
			}

			loginStatus.setLoginStatus(LoginStatus.LOGIN_STATUS_SUCCESS_CODE);
			loginStatus.setStatusString(LoginStatus.LOGIN_STATUS_SUCCESS);
		} else {
			loginStatus.setLoginStatus(LoginStatus.LOGIN_STATUS_FAILURE_CODE);
			loginStatus.setStatusString(LoginStatus.LOGIN_STATUS_FAILURE);
		}
		return loginStatus;
	}

}
