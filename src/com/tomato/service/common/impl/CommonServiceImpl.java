package com.tomato.service.common.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tomato.dao.common.CommonDao;
import com.tomato.service.common.CommonService;

@Service
@Transactional
public class CommonServiceImpl implements CommonService {

	private static final Logger LOGGER = Logger.getLogger(CommonServiceImpl.class);

	@Autowired
	HttpServletRequest request;
	
	@Autowired
	private CommonDao commonDao;

	@Override
	public boolean save(Object obj) {
		return commonDao.save(obj);
	}

	@Override
	public boolean isExists(String beanName, String whereCondition) {
		return commonDao.isExists(beanName, whereCondition);
	}

	@Override
	public <T> T get(Class<T> t, Serializable key) {
		return commonDao.get(t, key);
	}

	@Override
	public List<HashMap<String, String>> getSpecificColumn(String beanName, List<String> columns,
			String whereCondition) {
		return commonDao.getSpecificColumn(beanName, columns, whereCondition);
	}

	@Override
	public String generateDropdownOptions(String beanName, String key, String value, String whereCondition) {

		String sropDownOptions = "<option value=''>--select--</option>";
		ArrayList<String> columns = new ArrayList<>();
		columns.add(key);
		columns.add(value);

		Set<String> prevData = new HashSet<>();

		List<HashMap<String, String>> data = getSpecificColumn(beanName, columns, whereCondition);
		Iterator<HashMap<String, String>> itr = data.iterator();
		while (itr.hasNext()) {
			HashMap<String, String> map = itr.next();
			if (!prevData.contains(map.get(key))) {
				sropDownOptions = sropDownOptions + " <option value='" + map.get(key) + "'>" + map.get(value)
						+ "</option>";
				prevData.add(map.get(key));
			}
		}

		return sropDownOptions;
	}

	@Override
	public <T> List<T> getAllData(Class<T> T) {
		return commonDao.getAllData(T);
	}

	@Override
	public <T> List<T> getDataPagination(Class<T> T, int startIndex, int maxResult) {
		return commonDao.getDataPagination(T, startIndex, maxResult);
	}

	@Override
	public <T> List<T> getDataByWhereCondition(Class<T> T, String whereCond) {
		return commonDao.getDataByWhereCondition(T, whereCond);
	}

	@Override
	public <T> void deleteData(Class<T> T, String whereCondition) {
		commonDao.deleteData(T, whereCondition);
	}

	@Override
	public <T> List<String> getSingleColumnList(Class<T> T, String columnName, String whereCondition) {
		List<String> list = new ArrayList<>();

		List<Object> objList = commonDao.getSingleColumnList(T, columnName, whereCondition);
		if (objList != null && objList.size() > 0) {
			Iterator<Object> itr = objList.iterator();
			while (itr.hasNext()) {
				list.add(String.valueOf(itr.next()));
			}
		}

		return list;
	}

	@Override
	public <T> void updateColumns(Class<T> T, List<String[]> columns, String whereCondition) {
		commonDao.updateColumns(T, columns, whereCondition);
	}


	@Override
	public void runHQLDMLQuery(String query) {
		commonDao.runHQLDMLQuery(query);
	}
	
}
