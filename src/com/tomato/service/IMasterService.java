package com.tomato.service;

import java.util.List;
import java.util.Map;

import com.tomato.auth.bean.LoginStatus;
import com.tomato.bean.MasterData;
import com.tomato.bean.MenuUrlBean;
import com.tomato.bean.UserBean;
import com.tomato.util.MasterDataEnum;

public interface IMasterService {

	public List<MasterData> getMasterData(MasterDataEnum type);

	public Map<String, List<MenuUrlBean>> getUserMenuDetails(int userId);

	public boolean checkMenuAccess(String URL, int userId);

	public MenuUrlBean getMenuURLBeanByMenuURL(String URL);

	public Map<String, List<MenuUrlBean>> getAllMenuDetails();

	public List<MenuUrlBean> getAllMenuRaw();

	public LoginStatus checkLogin(String userId, String password, UserBean userBean);

}
