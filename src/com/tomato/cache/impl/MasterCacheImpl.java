package com.tomato.cache.impl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tomato.bean.MasterData;
import com.tomato.cache.MasterCache;
import com.tomato.dao.IMasterDao;
import com.tomato.util.MasterDataEnum;

@Service
@Transactional
public class MasterCacheImpl implements MasterCache {

	protected static Logger logger = Logger.getLogger(MasterCacheImpl.class);

	@Autowired
	private IMasterDao objMasterDao;

	private static List<MasterData> statusList;
	public Map<Integer, String> allMasterData;
	
	@PostConstruct
	@Override
	public void init() {
		statusList = objMasterDao.getMasterData(MasterDataEnum.STATUS);
		allMasterData = new ConcurrentHashMap<Integer, String>();

		List<MasterData> masterDataList = objMasterDao.getAllMasterData();
		for (MasterData data : masterDataList) {
			saveOrUpdate(allMasterData, data);
		}

	}

	@Override
	public List<MasterData> getMasterData(MasterDataEnum type) {

		switch (type) {
		case STATUS:
			return getStatusList();
		default:
			return null;
		}
	}
	
	@Override
	public void saveOrUpdate(Map<Integer, String> map, MasterData masterData) {
		map.put(masterData.getCode(), masterData.getDesc());
	}

	@Override
	public List<MasterData> getStatusList() {
		return statusList;
	}

	@Override
	public String getDataById(String code) {
		if (code != null && !code.equals("")) {
			return this.allMasterData.get(Integer.parseInt(code));
		} else {
			return "";
		}
	}

}
