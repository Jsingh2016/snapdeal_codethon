package com.tomato.cache;

import java.util.List;
import java.util.Map;

import com.tomato.bean.MasterData;
import com.tomato.util.MasterDataEnum;

public interface MasterCache {

	public void saveOrUpdate(Map<Integer, String> map,MasterData department);
	
	public List<MasterData> getStatusList();
	
	public List<MasterData> getMasterData(MasterDataEnum type);
	
	public String getDataById(String statusCode);
	
	public void init();
	
}
