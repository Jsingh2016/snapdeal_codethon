package com.tomato.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="master_data")
public class MasterData implements Serializable{
	
	private static final long serialVersionUID = -723583058586873479L;
	
	@Id
	@Column(name="code")
	private int code;
	
	@Column(name="desc")
	private String desc;
	
	@Column(name="type")
	private String type;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "MasterData [code=" + code + ", desc=" + desc + ", type=" + type
				+ "]";
	}
	

}
