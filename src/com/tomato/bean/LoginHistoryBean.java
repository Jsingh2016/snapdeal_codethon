package com.tomato.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="login_history")
public class LoginHistoryBean extends SuperBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8425365878492998703L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@Column(name="user_id")
	private int userId;
	
	@Column(name="loged_in_country")
	private String loggedInCountry;
	
	@Column(name="user_country")
	private String userCountry;
	
	@Column(name="ip_address")
	private String ipAddress;
	
	@Column(name="session_id")
	private String sessionId;

	@Column(name="address")
	private String address;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getLoggedInCountry() {
		return loggedInCountry;
	}

	public void setLoggedInCountry(String loggedInCountry) {
		this.loggedInCountry = loggedInCountry;
	}

	public String getUserCountry() {
		return userCountry;
	}

	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
}
