package com.tomato.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnore;

@MappedSuperclass
public abstract class SuperBean implements Serializable
{
	private static final long serialVersionUID = 1l;
	
	@JsonIgnore
	@Column(name = "created_date", updatable=false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate = new Date();
 
	@JsonIgnore
    @Column(name = "modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedDate = new Date();
 
	@JsonIgnore
    @Column(name = "created_by", updatable=false)
    private int createdBy ;
 
	@JsonIgnore
    @Column(name = "modified_by")
    private int modifiedBy ;
	
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public int getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(int modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}