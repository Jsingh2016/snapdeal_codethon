package com.tomato.bean;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.format.annotation.DateTimeFormat;

import com.tomato.util.TomatoConstants;

@Entity
@Table(name="user")
public class UserBean extends SuperBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4515534919408475215L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(name="user_name", unique=true)
	private String userName;
	
	@Column(name="nick_name")
	private String nickName;
	
	@Column(name="password")
	private String password;
	
	@Column(name="recovery_email")
	private String recoveryEmail;
	
	@Column(name="mobile")
	private long mobile;
	
	@Column(name="status", columnDefinition="INT(11) DEFAULT 0")
	private int status;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="status", insertable = false,updatable=false)
	@NotFound(action = NotFoundAction.IGNORE)
	private MasterData statusBean ;
	
	@Column(name="password_expiry_days", columnDefinition="INT(11) DEFAULT 0")
	private int passwordExpiryDays;
	
	@Temporal(TemporalType.DATE)
	@Column(name="registration_date")
	@DateTimeFormat(pattern=TomatoConstants.DISPLAY_DATE_FORMAT)
	private Date registrationDate;
	
	@Column(name="user_type", columnDefinition="INT(11) DEFAULT 0")
	private int userType;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="user_type", insertable = false,updatable=false)
	@NotFound(action = NotFoundAction.IGNORE)
	private MasterData userTypeBean ;

	@Transient
	private Collection<Integer> menuIds;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRecoveryEmail() {
		return recoveryEmail;
	}

	public void setRecoveryEmail(String recoveryEmail) {
		this.recoveryEmail = recoveryEmail;
	}

	public long getMobile() {
		return mobile;
	}

	public void setMobile(long mobile) {
		this.mobile = mobile;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public MasterData getStatusBean() {
		return statusBean;
	}

	public void setStatusBean(MasterData statusBean) {
		this.statusBean = statusBean;
	}

	public int getPasswordExpiryDays() {
		return passwordExpiryDays;
	}

	public void setPasswordExpiryDays(int passwordExpiryDays) {
		this.passwordExpiryDays = passwordExpiryDays;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public MasterData getUserTypeBean() {
		return userTypeBean;
	}

	public void setUserTypeBean(MasterData userTypeBean) {
		this.userTypeBean = userTypeBean;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Collection<Integer> getMenuIds() {
		return menuIds;
	}

	public void setMenuIds(Collection<Integer> menuIds) {
		this.menuIds = menuIds;
	}
	
	
	
	
}
