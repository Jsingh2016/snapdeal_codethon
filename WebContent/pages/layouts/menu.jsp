<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="main-container container-fluid">
	<a class="menu-toggler" id="menu-toggler" href="#"> <span
		class="menu-text"></span></a>
	<div class="sidebar" id="sidebar">
		<ul class="nav nav-list">
			<li><a href="${pageContext.request.contextPath}/dashboard.html"><i
					class="icon-dashboard"></i> <span class="menu-text">
						Dashboard </span></a></li>
			<li class=""><a href="#" class="dropdown-toggle"><i
					class="icon-desktop"></i> <span class="menu-text">Master</span> <b
					class="arrow icon-angle-down"></b></a>
				<ul style="display: block;" class="submenu">

					<li class=""><a
						href="${pageContext.request.contextPath}/user/userList.html"
						target="_self"> <i class="icon-double-angle-right"></i> User
					</a></li>

				</ul></li>

		</ul>
		<div class="sidebar-collapse" id="sidebar-collapse">
			<i class="icon-double-angle-left"></i>
		</div>
	</div>
</div>