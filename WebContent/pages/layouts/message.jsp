<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div style="text-align: center;">
	<c:if test="${not empty msg}">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<div class="alert alert-success" role="alert" id="msg">${msg }</div>
	</c:if>
	<c:if test="${not empty error}">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<div class="alert alert-error" role="alert" id="msg">${error}</div>
	</c:if>
	<c:if test="${not empty success}">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<div class="alert alert-success" role="alert" id="msg">${success }</div>
	</c:if>
	
</div>
