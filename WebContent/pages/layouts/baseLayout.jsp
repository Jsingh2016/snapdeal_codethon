<%@page import="com.tomato.util.TransportConstants"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<title><tiles:insertAttribute name="title" /></title>
<!-- <script type="text/javascript" src="assets/js/jquery-2.0.3.min.js"></script> -->
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" />
    <script type="text/javascript"
        src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript"
        src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>

<meta name="description" content="overview &amp; stats" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!--basic styles-->

<link href="<c:url value="/assets/css/bootstrap.min.css"></c:url>" rel="stylesheet" />
<link href="<c:url value="/assets/css/bootstrap-responsive.min.css"></c:url>" rel="stylesheet" />
<link rel="stylesheet" href="<c:url value="/assets/css/font-awesome.min.css"></c:url>" />

<link rel="stylesheet" href="<c:url value="/assets/css/bootstrap-duallistbox.css"></c:url>"  />
<!--[if IE 7]>
		  <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

<!--page specific plugin styles-->

<!--fonts-->

<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />

<!--ace styles-->

<!-- <link rel="stylesheet" href="assets/css/datepicker.css" /> -->

<link rel="stylesheet" href="<c:url value="/assets/css/ace.min.css"></c:url>" />
<link rel="stylesheet" href="<c:url value="/assets/css/ace-responsive.min.css"></c:url>" />
<link rel="stylesheet" href="<c:url value="/assets/css/ace-skins.min.css"></c:url>" />
<link rel="stylesheet" href="<c:url value="/assets/css/datepicker.css"></c:url>" />
<!--[if lte IE 8]>
		  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
		<![endif]-->

<!--inline styles related to this page-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="<c:url value="/js/menu.js"></c:url>" type="text/javascript"></script>

<script src="<c:url value="/assets/js/jquery.dataTables.min.js"></c:url>"></script>
	<script src="<c:url value="/assets/js/jquery.dataTables.bootstrap.js"></c:url>"></script>

	<!--ace scripts-->

	<script src="<c:url value="/assets/js/ace-elements.min.js"></c:url>"></script>
	<script src="<c:url value="/assets/js/ace.min.js"></c:url>"></script>
	<script src="<c:url value="/js/validation.js"></c:url>"></script>
<script src="<c:url value="/assets/js/jquery.bootstrap-duallistbox.js"></c:url>" ></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$.ajaxSetup({
			 beforeSend: function() {
				$('#loader').fadeIn();
			 },
			 complete: function() {
				 checkPriv();
				 $('#loader').fadeOut();
			 },
			 success: function() {
				 checkPriv();
				 $('#loader').fadeOut();
			 },
			 error : function(jqXHR, textStatus, errorThrown) {
				 checkPriv();
				 $('#loader').fadeOut();
			    if (jqXHR.status == 403) {
			    	window.location.href = '<%=TransportConstants.ACCESS_DENIED_URL%>';
		        } else {
		            alert("Error: " + textStatus + ": " + errorThrown);
		        }
			 }
			});
		
		var countdown;

		countdown = setTimeout(function() {
			$("#msg").hide();
			$("#msgErr").hide();
			$(".close").hide();
		}, 5000);
		
		
	});
	

	function deleteFile(fileId, obj){
		if(confirm("Are you sure you want to deete this record?")){
			$.ajax({url : "${pageContext.request.contextPath}/master/DeleteAttachment.html?attachmentId="+ fileId,
				type : 'POST',
				data : "",
				success : function(data) {
					if(data == 'success'){
						$(obj).parent().parent().remove();
					}
				},
				error : function(data,status, er) {
					alert(er);
				}
			});
		}
	}
	
	
	function checkPriv(){
		var deletePriv = $("#deletePriv").val();
		var editPriv = $("#editPriv").val();
		//alert(editPriv + " - " + deletePriv);
		if(deletePriv == 'N'){
			if (typeof $("#deleteLink") !== "undefined" && $("#deleteLink") != undefined && $("#deleteLink") != 'undefined') {
				$("a#deleteLink").hide();
			}
		}
		if(editPriv == 'N'){
			if (typeof $("#editLink") !== "undefined" && $("#editLink") != undefined && $("#editLink") != 'undefined') {
				$("a#editLink").hide();
			} 
		}
		
		
		/* else{
			if (typeof $("#editLink") !== "undefined" && $("#editLink") != undefined && $("#editLink") != 'undefined') {
				$("a#editLink").show();
			}
			if (typeof $("#deleteLink") !== "undefined" && $("#deleteLink") != undefined && $("#deleteLink") != 'undefined') {
				$("a#deleteLink").show();
			}
		} */
	}
	
	</script>

</head>

<body>
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a href="#" class="logo"><img src="${pageContext.request.contextPath}/assets/Maclines LOGO.png" />
				</a>
				<ul class="nav ace-nav pull-right">
					
					<li class="light-blue"><a data-toggle="dropdown" href="#"
						class="dropdown-toggle"><img class="nav-user-photo"
							src="${pageContext.request.contextPath}/assets/avatars/user.jpg" alt="Jason Photo" /> <span
							class="user-info"><small>Welcome,</small><security:authentication property="principal.userName"/></span> <i
							class="icon-caret-down"></i>
					</a>
						<ul
							class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
							<li><a href="#"><i class="icon-user"></i>Profile</a>
							</li>
							<li class="divider"></li>
							
							<li><a href="${pageContext.request.contextPath}/logout.html"><i class="icon-off"></i>Logout</a>
							</li>
						</ul></li>
				</ul>
			</div>
		</div>
	</div>
	<tiles:insertAttribute name="menu" />
	
	<!-- <script>
		var domainAppNameUrl='<c:url value="/"></c:url>';
		sidebar1();
	</script> -->
	<div class="main-content">
		<tiles:insertAttribute name="body" />
		<div class="footer">
			<tiles:insertAttribute name="footer" />
		</div>
	</div>

	<div class="modal" id="loader" style="display: none">
        <div class="center">
            <img alt="" src="<c:url value="/assets/img/loader.gif"></c:url>" />
        </div>
    </div>

	<!-- <script
		src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
 -->
	<!--<![endif]-->

	<!--[if IE]>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->

	<!--[if !IE]>-->
<!-- 
	<script type="text/javascript">
		window.jQuery
				|| document
						.write("<script src='assets/js/jquery-2.0.3.min.js'>"
								+ "<"+"/script>");
	</script> -->

	<!--<![endif]-->

	<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

	<script type="text/javascript">
		if ("ontouchend" in document)
			document
					.write("<script src='<c:url value='/assets/js/jquery.mobile.custom.min.js'></c:url>'>"
							+ "<"+"/script>");
	</script>
	<script src="<c:url value='/assets/js/bootstrap.min.js'></c:url>"></script>

            <script type="text/javascript">
			$( document ).ready(function() {
						    			 
				var demo1 = $('select[name="menuIds"]').bootstrapDualListbox({infoTextFiltered: '<span class="label label-purple label-lg">Filtered</span>'});
				var container1 = demo1.bootstrapDualListbox('getContainer');
				container1.find('.btn').addClass('btn-white btn-info btn-bold');
			
			});
			</script>

</body>
</html>