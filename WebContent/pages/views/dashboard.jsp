<div class="breadcrumbs" id="breadcrumbs">
	<ul class="breadcrumb">
		<li><i class="icon-home home-icon"></i> <a href="#">Home</a> <span
			class="divider"> <i class="icon-angle-right arrow-icon"></i> </span>
		</li>
		<li class="active">Dashboard</li>
	</ul>
</div>

<div class="page-content">
	<div class="row-fluid">
		<div class="span12">
			<!--PAGE CONTENT BEGINS-->

			<div class="row-fluid">
				<div class="span4 widget-container-span">
					<div class="widget-box">
						<div class="widget-header header-color-orange">
							<h5 class="bigger lighter">
								<i class="icon-table"></i> Dashboard 1
							</h5>
						</div>

						<div class="widget-body">
							<div class="widget-main no-padding">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th><i class="icon-user"></i> User</th>

											<th><i>@</i> Email</th>
											<th class="hidden-480">Status</th>
										</tr>
									</thead>

									<tbody>
										<tr>
											<td class="">Alex</td>

											<td><a href="#">alex@email.com</a></td>

											<td class="hidden-480"><span class="label label-warning">Pending</span>
											</td>
										</tr>

										<tr>
											<td class="">Fred</td>

											<td><a href="#">fred@email.com</a></td>

											<td class="hidden-480"><span
												class="label label-success arrowed-in arrowed-in-right">Approved</span>
											</td>
										</tr>

										<tr>
											<td class="">Jack</td>

											<td><a href="#">jack@email.com</a></td>


											<td class="hidden-480"><span class="label label-warning">Pending</span>
											</td>
										</tr>

										<tr>
											<td class="">John</td>

											<td><a href="#">john@email.com</a></td>

											<td class="hidden-480"><span
												class="label label-inverse arrowed">Blocked</span></td>
										</tr>

										<tr>
											<td class="">James</td>

											<td><a href="#">james@email.com</a></td>

											<td class="hidden-480"><span
												class="label label-info arrowed-in arrowed-in-right">Online</span>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!--/span-->
				<div class="span4 widget-container-span">
					<div class="widget-box">
						<div class="widget-header header-color-blue">
							<h5 class="bigger lighter">
								<i class="icon-table"></i> Dashboard 2
							</h5>
						</div>

						<div class="widget-body">
							<div class="widget-main no-padding">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th><i class="icon-user"></i> User</th>

											<th><i>@</i> Email</th>
											<th class="hidden-480">Status</th>
										</tr>
									</thead>

									<tbody>
										<tr>
											<td class="">Alex</td>

											<td><a href="#">alex@email.com</a></td>

											<td class="hidden-480"><span class="label label-warning">Pending</span>
											</td>
										</tr>

										<tr>
											<td class="">Fred</td>

											<td><a href="#">fred@email.com</a></td>

											<td class="hidden-480"><span
												class="label label-success arrowed-in arrowed-in-right">Approved</span>
											</td>
										</tr>

										<tr>
											<td class="">Jack</td>

											<td><a href="#">jack@email.com</a></td>


											<td class="hidden-480"><span class="label label-warning">Pending</span>
											</td>
										</tr>

										<tr>
											<td class="">John</td>

											<td><a href="#">john@email.com</a></td>

											<td class="hidden-480"><span
												class="label label-inverse arrowed">Blocked</span></td>
										</tr>

										<tr>
											<td class="">James</td>

											<td><a href="#">james@email.com</a></td>

											<td class="hidden-480"><span
												class="label label-info arrowed-in arrowed-in-right">Online</span>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="span4 widget-container-span">
					<div class="widget-box">
						<div class="widget-header header-color-green">
							<h5 class="bigger lighter">
								<i class="icon-table"></i> Dashboard 3
							</h5>
						</div>

						<div class="widget-body">
							<div class="widget-main no-padding">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th><i class="icon-user"></i> User</th>

											<th><i>@</i> Email</th>
											<th class="hidden-480">Status</th>
										</tr>
									</thead>

									<tbody>
										<tr>
											<td class="">Alex</td>

											<td><a href="#">alex@email.com</a></td>

											<td class="hidden-480"><span class="label label-warning">Pending</span>
											</td>
										</tr>

										<tr>
											<td class="">Fred</td>

											<td><a href="#">fred@email.com</a></td>

											<td class="hidden-480"><span
												class="label label-success arrowed-in arrowed-in-right">Approved</span>
											</td>
										</tr>

										<tr>
											<td class="">Jack</td>

											<td><a href="#">jack@email.com</a></td>


											<td class="hidden-480"><span class="label label-warning">Pending</span>
											</td>
										</tr>

										<tr>
											<td class="">John</td>

											<td><a href="#">john@email.com</a></td>

											<td class="hidden-480"><span
												class="label label-inverse arrowed">Blocked</span></td>
										</tr>

										<tr>
											<td class="">James</td>

											<td><a href="#">james@email.com</a></td>

											<td class="hidden-480"><span
												class="label label-info arrowed-in arrowed-in-right">Online</span>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--/row-->

			<div class="space-24"></div>
			<div class="row-fluid">
				<div class="span4 widget-container-span">
					<div class="widget-box">
						<div class="widget-header header-color-orange">
							<h5 class="bigger lighter">
								<i class="icon-table"></i> Dashboard 1
							</h5>
						</div>

						<div class="widget-body">
							<div class="widget-main no-padding">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th><i class="icon-user"></i> User</th>

											<th><i>@</i> Email</th>
											<th class="hidden-480">Status</th>
										</tr>
									</thead>

									<tbody>
										<tr>
											<td class="">Alex</td>

											<td><a href="#">alex@email.com</a></td>

											<td class="hidden-480"><span class="label label-warning">Pending</span>
											</td>
										</tr>

										<tr>
											<td class="">Fred</td>

											<td><a href="#">fred@email.com</a></td>

											<td class="hidden-480"><span
												class="label label-success arrowed-in arrowed-in-right">Approved</span>
											</td>
										</tr>

										<tr>
											<td class="">Jack</td>

											<td><a href="#">jack@email.com</a></td>


											<td class="hidden-480"><span class="label label-warning">Pending</span>
											</td>
										</tr>

										<tr>
											<td class="">John</td>

											<td><a href="#">john@email.com</a></td>

											<td class="hidden-480"><span
												class="label label-inverse arrowed">Blocked</span></td>
										</tr>

										<tr>
											<td class="">James</td>

											<td><a href="#">james@email.com</a></td>

											<td class="hidden-480"><span
												class="label label-info arrowed-in arrowed-in-right">Online</span>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!--/span-->
				<div class="span4 widget-container-span">
					<div class="widget-box">
						<div class="widget-header header-color-blue">
							<h5 class="bigger lighter">
								<i class="icon-table"></i> Dashboard 2
							</h5>
						</div>

						<div class="widget-body">
							<div class="widget-main no-padding">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th><i class="icon-user"></i> User</th>

											<th><i>@</i> Email</th>
											<th class="hidden-480">Status</th>
										</tr>
									</thead>

									<tbody>
										<tr>
											<td class="">Alex</td>

											<td><a href="#">alex@email.com</a></td>

											<td class="hidden-480"><span class="label label-warning">Pending</span>
											</td>
										</tr>

										<tr>
											<td class="">Fred</td>

											<td><a href="#">fred@email.com</a></td>

											<td class="hidden-480"><span
												class="label label-success arrowed-in arrowed-in-right">Approved</span>
											</td>
										</tr>

										<tr>
											<td class="">Jack</td>

											<td><a href="#">jack@email.com</a></td>


											<td class="hidden-480"><span class="label label-warning">Pending</span>
											</td>
										</tr>

										<tr>
											<td class="">John</td>

											<td><a href="#">john@email.com</a></td>

											<td class="hidden-480"><span
												class="label label-inverse arrowed">Blocked</span></td>
										</tr>

										<tr>
											<td class="">James</td>

											<td><a href="#">james@email.com</a></td>

											<td class="hidden-480"><span
												class="label label-info arrowed-in arrowed-in-right">Online</span>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="span4 widget-container-span">
					<div class="widget-box">
						<div class="widget-header header-color-green">
							<h5 class="bigger lighter">
								<i class="icon-table"></i> Dashboard 3
							</h5>
						</div>

						<div class="widget-body">
							<div class="widget-main no-padding">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th><i class="icon-user"></i> User</th>

											<th><i>@</i> Email</th>
											<th class="hidden-480">Status</th>
										</tr>
									</thead>

									<tbody>
										<tr>
											<td class="">Alex</td>

											<td><a href="#">alex@email.com</a></td>

											<td class="hidden-480"><span class="label label-warning">Pending</span>
											</td>
										</tr>

										<tr>
											<td class="">Fred</td>

											<td><a href="#">fred@email.com</a></td>

											<td class="hidden-480"><span
												class="label label-success arrowed-in arrowed-in-right">Approved</span>
											</td>
										</tr>

										<tr>
											<td class="">Jack</td>

											<td><a href="#">jack@email.com</a></td>


											<td class="hidden-480"><span class="label label-warning">Pending</span>
											</td>
										</tr>

										<tr>
											<td class="">John</td>

											<td><a href="#">john@email.com</a></td>

											<td class="hidden-480"><span
												class="label label-inverse arrowed">Blocked</span></td>
										</tr>

										<tr>
											<td class="">James</td>

											<td><a href="#">james@email.com</a></td>

											<td class="hidden-480"><span
												class="label label-info arrowed-in arrowed-in-right">Online</span>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="hr dotted"></div>
