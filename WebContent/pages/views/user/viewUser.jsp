<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript">
	$(document).ready(function() {

		$("#userCancel").click(function() {
			window.location.href = "userList.html";
		});
	});
</script>
<div class="breadcrumbs" id="breadcrumbs">
	<ul class="breadcrumb">
		<li><i class="icon-user home-icon"></i> <a href="#">Master</a> <span
			class="divider"> <i class="icon-angle-right arrow-icon"></i>
		</span></li>
		<li class="active">View User</li>
	</ul>
</div>
<div class="page-content">
	<div class="row-fluid">
		<div class="span8">
			<div class="widget-box">
				<div class="widget-header">
					<h4>View User</h4>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<div class="row-fluid">
							<div class="control-group">
								<div class="span12">
									<div class="profile-user-info profile-user-info-striped">
										<div class="profile-info-row">
											<div class="profile-info-name">User Name:</div>

											<div class="profile-info-value">
												<span id="username">${userBean.userName}</span>
											</div>
										</div>
										<div class="profile-info-row">
											<div class="profile-info-name">User ID</div>

											<div class="profile-info-value">
												<span id="username">${userBean.id}</span>
											</div>
										</div>
										<div class="profile-info-row">
											<div class="profile-info-name">Password</div>

											<div class="profile-info-value">
												<span id="username">${userBean.password}</span>
											</div>
										</div>
										<div class="profile-info-row">
											<div class="profile-info-name">E-mail</div>

											<div class="profile-info-value">
												<span id="username">${userBean.recoveryEmail}</span>
											</div>
										</div>
										<div class="profile-info-row">
											<div class="profile-info-name">Mobile</div>

											<div class="profile-info-value">
												<span id="username">${userBean.mobile}</span>
											</div>
										</div>
										
										<div class="profile-info-row">
											<div class="profile-info-name">Status</div>

											<div class="profile-info-value">
												<span id="username">${userBean.statusBean.desc}</span>
											</div>
										</div>
										
										<%-- <div class="profile-info-row">
											<div class="profile-info-name">URL Access</div>
											<div class="profile-info-value" style="height: 160px; overflow:auto">
											<table class="table table-striped table-bordered table-hover">
											<c:forTokens items="${menusName }" delims="," var="name">
											<tr><td>
												<span id="roles">
   														<c:out value="${name}"/>
											    </span>
											    </td>
										    </tr>
											</c:forTokens>
											</table>
											</div>
										</div> --%>
										<div class="form-actions" align="center">
											<button id="userCancel" class="btn btn-small btn-info"
												type="reset" align="center">
												<i class="icon-undo bigger-110"></i> Back
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>