<%@page import="com.tomato.bean.UserBean"%>
<%@page import="com.tomato.util.DateUtil"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript">
function deleteUser(userId){
	if(confirm("Are you sure you want to delete User?")){
		$.ajax({
    		type : "POST",
    		url : "${pageContext. request. contextPath}/user/deleteUser.html?id="+userId,
    		data : "",
    		datatype : 'json',
    		contentType : 'application/json',
    		async : true,
    		cache : false,
    		success : function(response) {
    			location.reload(true);
    		}
    	});
	}
}
</script>

<div class="breadcrumbs" id="breadcrumbs">
	<ul class="breadcrumb">
		<li><i class="icon-user home-icon"></i> <a href="#">Master</a> <span
			class="divider"> <i class="icon-angle-right arrow-icon"></i>
		</span></li>
		<li class="active">Users</li>
	</ul>
</div>
<div class="page-content">
	<div class="row-fluid">
		<div class="span12">
			<jsp:include page="/pages/layouts/message.jsp" flush="true"></jsp:include>
			<div class="table-header">
				User List 
				<a href="ExportUserExcel.html"><button
						class="btn btn-small btn-light pull-right" type="button">
						Export to Excel</button> </a>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="ExportUserPDF.html"><button
						class="btn btn-small btn-light pull-right" type="button">
						Export to PDF</button> </a>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="getUserForm.html"><button
						class="btn btn-small btn-light pull-right" type="button">
						New User</button> </a>
			</div>
			<div class="row-fluid">
				<jsp:include page="/pages/layouts/message.jsp" flush="true"></jsp:include>
				<table id="userDatatable"
					class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>User Id</th>
							<th>User name</th>
							<th>Email</th>
							<th>Alias</th>
							<th>Mobile</th>
							<th>Registration Date</th>
							<th class="hidden-480">Status</th>

							<th>Action</th>
						</tr>
					</thead>

					<tbody>
						<c:forEach items="${userList}" var="user" varStatus="theCount">
									
						<tr>
							<td class="center"><label> <span class="lbl">${user.id }</span>
							</label></td>

							<td><a href="#">${user.userName }</a></td>
							<td>${user.recoveryEmail }</td>
							<td>${user.nickName }</td>
							<td>${user.mobile }</td>
							<td><%=DateUtil.displayDateinDDMMYYYY(((UserBean)pageContext.getAttribute("user")).getRegistrationDate()) %></td>
							<td class="hidden-phone">${user.statusBean.desc }</td>
							<td class="td-actions">
								<div class="hidden-phone visible-desktop action-buttons">
									<a class="blue" href="viewUser.html?id=${user.id }"> <i
										class="icon-zoom-in bigger-130"></i>
									</a> 
									<c:if test="${allGrn.status != 13 && allGrn.status != 14 }">
										<a class="green" id="editLink" href="editUser.html?id=${user.id }"> <i
											class="icon-pencil bigger-130"></i>
										</a>
										<a class="red" id="deleteLink" onclick="deleteUser(${user.id })" href="javascript:void(0);"> <i
											class="icon-trash bigger-130"></i>
										</a>
									</c:if>
									 
								</div>

								<div class="hidden-desktop visible-phone">
									<div class="inline position-relative">
										<button class="btn btn-minier btn-yellow dropdown-toggle"
											data-toggle="dropdown">
											<i class="icon-caret-down icon-only bigger-120"></i>
										</button>

										<ul
											class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">
											<li><a href="viewUser.html?id=${user.id }" class="tooltip-info"
												data-rel="tooltip" title="View"> <span class="blue">
														<i class="icon-zoom-in bigger-120"></i>
												</span>
											</a></li>
											<c:if test="${allGrn.status != 13 && allGrn.status != 14 }">
												<li><a id="editLink" href="editUser.html?id=${user.id }" class="tooltip-success"
													data-rel="tooltip" title="Edit"> <span class="green">
															<i class="icon-edit bigger-120"> test</i>
													</span>
													</a>
												</li>
												<li><a id="deleteLink" onclick="deleteUser(${user.id })" href="javascript:void(0);" class="tooltip-error"
												data-rel="tooltip" title="Delete"> <span class="red">
														<i class="icon-trash bigger-120"></i>
												</span>
											</a></li>
											</c:if>	
											
										</ul>
									</div>
								</div>
							</td>
						</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="hr dotted"></div>