<%@page import="com.transport.util.TransportConstants"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
	$(document).ready(function() {
		$("#userSubmit").click(function() {

			var failFlag = 0;

			$('[required]').each(function() {
				pass = validateRequired(this);
				if (!pass) {
					failFlag++;

				}
			});
			if (parseInt(failFlag) == 0) {
				$("#userForm").submit();
			}
		});
		$("#userCancel").click(function() {
			window.location.href = "userList.html";
		});
	});
</script>
<div class="breadcrumbs" id="breadcrumbs">
	<ul class="breadcrumb">
		<li><i class="icon-user home-icon"></i> <a href="#">Master</a> <span
			class="divider"> <i class="icon-angle-right arrow-icon"></i>
		</span></li>
		<li class="active">NEW User</li>
	</ul>
</div>
<div class="page-content">
	<div class="row-fluid">
		<div class="span8">
			<div class="widget-box">
				<div class="widget-header">
					<h4>Add User</h4>
				</div>
				<div class="widget-body">
					
					<div class="widget-main">
						<div class="row-fluid">
							<form:form class="form-horizontal" id="userForm"
								action="SaveUser.html" commandName="userBean">
								<form:hidden path="id" />
								<div class="control-group">
									<label class="control-label" for="form-field-1">User
										Name<font color="red">*:</font>
									</label>
									<div class="controls">
										<form:input required="required" path="userName"
											id="form-field-1" class="span10" placeholder="User name" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="form-field-1">Nick Name<font
										color="red">*:</font></label>
									<div class="controls">
										<form:input required="required" path="nickName"
											id="form-field-1" class="span10" placeholder="User id" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="form-field-1">Password<font
										color="red">*:</font><font color="red">:</font></label>
									<div class="controls">
										<form:password required="required" path="password"
											id="password" value="${userBean.password }" class="span10" placeholder="Password " />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="form-field-1">Recovery E-mail<font
										color="red">:</font></label>
									<div class="controls">
										<form:input required="required" path="recoveryEmail"
											id="form-field-1" class="span10" placeholder="E-mail" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="form-field-1">Mobile
										No.<font color="red">:</font>
									</label>
									<div class="controls">
										<form:input required="required" path="mobile"
											id="mobile" class="span10" placeholder="Mobile no. " />
									</div>
								</div>
								
								<div class="control-group">
									<label class="control-label" for="form-field-1">Status
									</label>
									<div class="controls">
										<form:select required="required" path="status"
											items="${statusList }" itemLabel="desc" itemValue="code"></form:select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="form-field-1">Password Expiry Days
										<font color="red">:</font>
									</label>
									<div class="controls">
										<form:input required="required" path="passwordExpiryDays"
											id="passwordExpiryDays" class="span10" placeholder="Mobile no. " />
									</div>
								</div>
								
								

								<%-- <div class="control-group">
									<label class="control-label" for="form-field-1">Roles </label>

									<div class="controls">
										<c:forEach var="role" items="${roleList}">
											<div class="checkbox">
												<label><input type="checkbox" value="${role.id}"
													<c:if test="${fn:contains(userBean.roleIds, role.id)}">checked="checked"</c:if>
													name="roleIds"><span class="lbl">
														${role.name}</span></label>
											</div>
										</c:forEach>
									</div>
								</div> --%>
								<div class="control-group">
									<label class=" control-label"> URL Access: </label>
									<div class="controls">
										<!-- #section:plugins/input.duallist -->
										<select multiple="multiple" size="10" name="menuIds"
											id="duallist">
											<c:forEach var="var" items="${menuList}">
												<c:set value="" var="test"></c:set>
												<c:forEach items="${userBean.menuIds}" var='userMenuId'>
													<c:if test="${userMenuId ==  var.menuId}">
														<c:set var="test" value="selected='selected'"></c:set>
													</c:if>	
												</c:forEach>
												<option value="${var.menuId}" ${test }>${var.menuDesc}(${var.parentInfo})</option>
											</c:forEach>
										</select>

										<div class="hr hr-16 hr-dotted"></div>
									</div>
								</div>

								<div class="form-actions">
									<button id="userSubmit" class="btn btn-small btn-info"
										type="button">
										<i class="icon-ok bigger-110"></i> Submit
									</button>
									     
									<button id="userCancel" class="btn btn-small btn-danger"
										type="reset">
										<i class="icon-undo bigger-110"></i> Cancel
									</button>
								</div>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>