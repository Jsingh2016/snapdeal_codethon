<%@ page trimDirectiveWhitespaces="true" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<body>
		<div id="mother">
			<div id="errorBox">
				<div id="errorText">
				<img alt="" src="<c:url value='/images/access-denied.png'></c:url>" height="200px" width="200px"/>
					<h1>Sorry, Access is Denied</h1>
								<a href="<c:url value='/dashboard.html'></c:url>" title="Home..."><img src="<c:url value='/assets/images/home_w.png'></c:url>" height="50px" width="50px">	</a>
				</div>

			</div>
		</div>
	</body>
</html>
