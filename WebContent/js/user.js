/*var formObject={action :""};*/
var objDataList= new Array();
var iModifiedRecordCnt=0;
var asInitVals = new Array();
var oTable=null;
var displayStart = 0;
var displayLength =10;


$(document).ready(function() {
	fnFetch();
});




function fnFetch() {
	$.ajax({
		type : "POST",
		url : "userAjax.html",
		data : null,
		datatype : 'json',
		contentType : 'application/json',
		async : true,
		cache : false,
		/*
		 * beforeSend: function(x) { fnShowProgress(); },
		 */
		success : function(response) {
			fnBindJQueryDataTable(response);
		}
	});

}

function fnBindJQueryDataTable(dataArray){
	if(oTable!=null){
		displayStart = oTable.fnSettings()._iDisplayStart;
		displayLength = oTable.fnSettings()._iDisplayLength;
		oTable.fnClearTable(this);
		oTable.fnDestroy();
		nRow=null;
		oTable=null;
	}
	var dColumns=[{ "mDataProp": "userName","sType": "string","sClass":"alignCenter","sWidth":"5%","bSortable": true},
	              { "mDataProp": "userId","sType": "string","sClass":"alignCenter","sWidth":"5%","bSortable": true},
	              { "mDataProp": "emailId","sType": "string","sClass":"alignCenter","sWidth":"5%","bSortable": true},
				{ "mDataProp": "mobileNo" ,"sType": "string","sClass":"alignCenter","sWidth":"5%","bSortable": true},
				{ "mDataProp": "statusBean.desc" ,"sType": "string","sClass":"alignCenter","sWidth":"6%","bSortable": true},
				{ "mDataProp": "action","sType": "string","sClass":"alignCenter","sWidth":"8%","bSortable": true}
				];
					
	oTable = $('#userDatatable').dataTable({
		"iDisplayStart": displayStart,
		"iDisplayLength": displayLength,
		"aaData": dataArray,
		"bAutoWidth": false,
		"bPaginate": true,
		"bSearchable": true,
		"bDeferRender": true,
		"sPaginationType": "full_numbers",
        /* "sScrollY": "250px",
         "bScrollCollapse": true,*/
         "fnRowCallback": function (nRow, aData, iDisplayIndex) {
        	
         },
         "aoColumns": dColumns
	});
	
	//$("#area_code_info").hide();
	//oTable.fnDraw(false);
	//$("#area_code_info").hide();
	
	 /*$.each($('.dataTables_wrapper >table >thead>tr>th'),function(){
			$(this).removeClass("alignRight");
			$(this).removeClass("alignLeft");
			$(this).removeClass("alignCenter");
			$(this).addClass("alignCenter");
	});
	 
	 $.each($('.dataTables_wrapper>table >tfoot>tr>th'),function(){
			$(this).removeClass("alignRight");
			$(this).removeClass("alignLeft");
			$(this).removeClass("alignCenter");
			$(this).addClass("alignCenter");
	 });*/
}