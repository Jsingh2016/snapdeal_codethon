<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8" />
<title>Login Page</title>

<meta name="description" content="User login page" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!--basic styles-->

<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/css/font-awesome.min.css" />

<!--[if IE 7]>
		  <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

<!--page specific plugin styles-->

<!--fonts-->

<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />

<!--ace styles-->

<link rel="stylesheet" href="assets/css/ace.min.css" />
<link rel="stylesheet" href="assets/css/ace-responsive.min.css" />
<link rel="stylesheet" href="assets/css/ace-skins.min.css" />
<link rel="stylesheet" href="assets/css/footer.css" />

<!--[if lte IE 8]>
		  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
		<![endif]-->

<!--inline styles related to this page-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body class="login-layout">
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<%-- <img src="${pageContext.request.contextPath}/assets/shivit.png" />
				<img
					src="${pageContext.request.contextPath}/assets/Maclines LOGO.png"
					class="pull-right" /> --%>
			</div>
		</div>
	</div>
	<div class="main-container container-fluid">
		<div class="main-content">
			<div class="row-fluid">
				<div class="span12">
					<div class="login-container">
						<div class="row-fluid">
							<div class="center">
								<h1>
									<span class="red">Welcome To Tomato</span> <span
										class="white"></span>
								</h1>

							</div>
						</div>

						<div class="space-6"></div>

						<div class="row-fluid">
							<div class="position-relative">
								<div id="login-box"
									class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger">
												<i class="icon-coffee green"></i> Please Enter Login
												Information
											</h4>
											<c:if test="${param.errorFailure}">
												<div class="alert alert-error">
													<button class="close" data-dismiss="alert"></button>
													<span>Enter Tomato Login Credential</span>
												</div>
											</c:if>
											<c:if test="${param.errorInactive}">
												<div class="alert alert-error">
													<button class="close" data-dismiss="alert"></button>
													<span>Enter correct Tomato Login Credential</span>
												</div>
											</c:if>
											<div class="space-6"></div>
											<form class="form-vertical login-form" action="<c:url value='/j_spring_security_check'></c:url>"
												id="loginForm" method="post">
												<fieldset>
													<label> <span
														class="block input-icon input-icon-right"> <input
															type="text" class="span12" name="j_username"
															placeholder="Username" /> <i class="icon-user"></i>
													</span>
													</label> 
													<label> <span
														class="block input-icon input-icon-right"> <input
															type="password" autocomplete="off" placeholder="Password"
															name="j_password" class="span12" /> <i class="icon-lock"></i>
													</span>
													</label>
													
													<div class="space"></div>

													<div class="clearfix">
														<label class="inline"> <input type="checkbox" />
															<span class="lbl"> Remember Me</span>
														</label>

														<button type="submit"
															class="width-35 pull-right btn btn-small btn-primary">
															<i class="icon-key"></i> Login
														</button>
													</div>

													<div class="space-4"></div>
												</fieldset>
											</form>




										</div>
										<!--/widget-main-->

										<div class="toolbar clearfix">
											<div>
												<a href="#" onclick="show_box('forgot-box'); return false;"
													class="forgot-password-link"> <i
													class="icon-arrow-left"></i> I forgot my password
												</a>
											</div>


										</div>
									</div>
									<!--/widget-body-->
								</div>
								<!--/login-box-->

								<div id="forgot-box" class="forgot-box widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header red lighter bigger">
												<i class="icon-key"></i> Retrieve Password
											</h4>

											<div class="space-6"></div>
											<p>Enter your email and to receive instructions</p>

											<form />
											<fieldset>
												<label> <span
													class="block input-icon input-icon-right"> <input
														type="email" class="span12" placeholder="Email" /> <i
														class="icon-envelope"></i>
												</span>
												</label>

												<div class="clearfix">
													<button onclick="return false;"
														class="width-35 pull-right btn btn-small btn-danger">
														<i class="icon-lightbulb"></i> Send Me!
													</button>
												</div>
											</fieldset>
											</form>
										</div>
										<!--/widget-main-->

										<div class="toolbar center">
											<a href="#" onclick="show_box('login-box'); return false;"
												class="back-to-login-link"> Back to login <i
												class="icon-arrow-right"></i>
											</a>
										</div>
									</div>
									<!--/widget-body-->
								</div>
								<!--/forgot-box-->


							</div>
							<!--/position-relative-->
						</div>
					</div>
				</div>
				<!--/.span-->
			</div>
			<!--/.row-fluid-->
		</div>
		<div class="footer hidden-phone visible-desktop">
			<div class="footer-inner">
				<div class="footer-content">
					<span class="bigger-120"> <span class="blue1 bolder">Shivit</span>
						<font color="white"> Technologies � 2015-2016</font>
					</span> &nbsp; &nbsp;

				</div>
			</div>
		</div>
	</div>
	<!--/.main-container-->

	<!--basic scripts-->

	<!--[if !IE]>-->

	<script
		src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

	<!--<![endif]-->

	<!--[if IE]>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->

	<!--[if !IE]>-->

	<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

	<!--<![endif]-->

	<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

	<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
	<script src="assets/js/bootstrap.min.js"></script>

	<!--page specific plugin scripts-->

	<!--ace scripts-->

	<script src="assets/js/ace-elements.min.js"></script>
	<script src="assets/js/ace.min.js"></script>

	<!--inline scripts related to this page-->

	<script type="text/javascript">
			function show_box(id) {
			 $('.widget-box.visible').removeClass('visible');
			 $('#'+id).addClass('visible');
			}
		</script>
</body>
</html>
